package ioasys.teste.android.empresas.classes;

import android.util.Log;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class Conexao {

    private static final String LOG_TAG = "IOASYS_LOG";
    private static int CODE_OK = 200;

    public static final String WEB_SERVICE_URL = "http://54.94.179.135:8090/api/v1/";
    public static final String LOGIN_USER_URL = "users/auth/sign_in";
    public static final String ENTERPRISE_INDEX_URL = "enterprises";
    public static final String SHOW_URL = "enterprises/{id}";
    public static final String ENTERPRESE_INDEX_FILTER_URL = "enterprises";

    private static Investor investor;
    private static DadosEmpresa dadosEmpresa;
    private static ListaEmpresas listaEmpresas;

    public static Investor fazerLogin(String email, String senha) {

        investor = null;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WEB_SERVICE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<Investor> call = service.doLogin(email, senha);
        try {
            Response<Investor> investorResponse = call.execute();

            Log.i(LOG_TAG, "Login->Response: " + investorResponse);
            Log.i(LOG_TAG, "Login->Response.successfull: " + investorResponse.isSuccessful());
            Log.i(LOG_TAG, "Login->Response.code: " + investorResponse.code());

            if (investorResponse.code() == CODE_OK) {
                investor = investorResponse.body();
                investor.setHeader_access_token(investorResponse.headers().get(Investor.TOKEN));
                investor.setHeader_client(investorResponse.headers().get(Investor.CLIENT));
                investor.setHeader_uid(investorResponse.headers().get(Investor.UID));
            }

        } catch (IOException e) {
            Log.i(LOG_TAG, "Login->Error:" + e.getMessage());
            Log.i(LOG_TAG, "Login->Error:" + e.toString());
        }

        return investor;
    }

    public static DadosEmpresa buscarDetalhesEmpresaById(String token, String client, String uid, String id) {

        dadosEmpresa = null;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WEB_SERVICE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<DadosEmpresa> call = service.buscarDetalheEmpresa(id, Investor.CONTENT_TYPE_JSON, token, client, uid);
        try {
            Response<DadosEmpresa> investorResponse = call.execute();

            Log.i(LOG_TAG, "DadosEmpresa->Response: " + investorResponse);
            Log.i(LOG_TAG, "DadosEmpresa->Response.successfull: " + investorResponse.isSuccessful());
            Log.i(LOG_TAG, "DadosEmpresa->Response.code: " + investorResponse.code());

            if (investorResponse.code() == CODE_OK) {

                dadosEmpresa = investorResponse.body();

            }

        } catch (IOException e) {
            Log.i(LOG_TAG, "DadosEmpresa->Error:" + e.getMessage());
            Log.i(LOG_TAG, "DadosEmpresa->Error:" + e.toString());
        }

        return dadosEmpresa;
    }

    public static ListaEmpresas buscarEmpresasByNome(String token, String client, String uid, String nome) {

        listaEmpresas = null;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WEB_SERVICE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<ListaEmpresas> call = service.buscarEmpresas(nome, Investor.CONTENT_TYPE_JSON, token, client, uid);
        try {
            Response<ListaEmpresas> investorResponse = call.execute();

            Log.i(LOG_TAG, "ListaEmpresas->Response: " + investorResponse);
            Log.i(LOG_TAG, "ListaEmpresas->Response.successfull: " + investorResponse.isSuccessful());
            Log.i(LOG_TAG, "ListaEmpresas->Response.code: " + investorResponse.code());

            if (investorResponse.code() == CODE_OK) {

                listaEmpresas = investorResponse.body();

            }

        } catch (IOException e) {
            Log.i(LOG_TAG, "ListaEmpresas->Error:" + e.getMessage());
            Log.i(LOG_TAG, "ListaEmpresas->Error:" + e.toString());
        }

        return listaEmpresas;
    }

    public interface APIService {

        @POST(Conexao.LOGIN_USER_URL)
        Call<Investor> doLogin(@Query("email") String email, @Query("password") String password);

        @GET(Conexao.SHOW_URL)
        Call<DadosEmpresa> buscarDetalheEmpresa(@Path("id") String id,
                                         @Header(Investor.CONTENT_TYPE) String content_type,
                                         @Header(Investor.TOKEN) String token,
                                         @Header(Investor.CLIENT) String client,
                                         @Header(Investor.UID) String uid);

        @GET(Conexao.ENTERPRISE_INDEX_URL)
        Call<ListaEmpresas> buscarEmpresas(@Query("name") String nome,
                                                @Header(Investor.CONTENT_TYPE) String content_type,
                                                @Header(Investor.TOKEN) String token,
                                                @Header(Investor.CLIENT) String client,
                                                @Header(Investor.UID) String uid);

        @GET(Conexao.ENTERPRESE_INDEX_FILTER_URL)
        Call<ListaEmpresas> buscarEmpresasFiltro(@Query("name") String nome, @Query("enterprise_types") String enterprise_type,
                                          @Header(Investor.CONTENT_TYPE) String content_type,
                                          @Header(Investor.TOKEN) String token,
                                          @Header(Investor.CLIENT) String client,
                                          @Header(Investor.UID) String uid);
    }
}
