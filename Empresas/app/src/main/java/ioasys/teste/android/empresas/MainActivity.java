package ioasys.teste.android.empresas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import ioasys.teste.android.empresas.activity.EmpresaDetalhesActivity;
import ioasys.teste.android.empresas.fragment.BuscaResultadoFragment;
import ioasys.teste.android.empresas.fragment.EmpresaDetalhesFragment;
import ioasys.teste.android.empresas.fragment.HomeFragment;
import ioasys.teste.android.empresas.fragment.LoginFragment;

public class MainActivity extends AppCompatActivity
        implements SearchView.OnQueryTextListener,
        MenuItemCompat.OnActionExpandListener,
        LoginFragment.LoginOnClick,
        BuscaResultadoFragment.BuscarOnClick {

    FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");

        mFragmentManager = getSupportFragmentManager();

        mudarFragment(new LoginFragment());

        // FAZER VERIFICACAO DE SE ESTA LOGADO OU NAO, SE SIM VAI PARA HomeFragment
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint(getString(R.string.action_search));

        MenuItemCompat.setOnActionExpandListener(searchItem, this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public static void hideActionBar(Activity activity) {
        if (activity instanceof MainActivity) {
             ActionBar actionBar = ((MainActivity) activity).getSupportActionBar();
            if (actionBar != null) {
                actionBar.hide();
            }
        }
    }

    public static void showActionBar(Activity activity) {
        if (activity instanceof MainActivity) {
            ActionBar actionBar = ((MainActivity) activity).getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
        }
    }

    public static void changeTitleActionBar(Activity activity, String title) {
        if (activity instanceof MainActivity) {
            ActionBar actionBar = ((MainActivity) activity).getSupportActionBar();

            if (actionBar != null) {
                actionBar.setTitle(title);
            }
        }
    }

    private void mudarFragment(Fragment fragment) {
        mFragmentManager.beginTransaction()
                .replace(R.id.frame_fragment, fragment).commit();
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        mudarFragment(new HomeFragment());
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        BuscaResultadoFragment buscaResultadoFragment = BuscaResultadoFragment.newInstance(query);
        mudarFragment(buscaResultadoFragment);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return true;
    }

    // ================================== Fragments Listeners ==========================================
    @Override
    public void login() {
        mudarFragment(new HomeFragment());
    }

    @Override
    public void itemEscolhidoClique(String id) {
        Intent it = new Intent(MainActivity.this, EmpresaDetalhesActivity.class);
        it.putExtra(EmpresaDetalhesFragment.EMPRESA, id);
        startActivity(it);
    }
}
