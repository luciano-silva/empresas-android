package ioasys.teste.android.empresas.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import ioasys.teste.android.empresas.R;
import ioasys.teste.android.empresas.activity.EmpresaDetalhesActivity;
import ioasys.teste.android.empresas.classes.Conexao;
import ioasys.teste.android.empresas.classes.DadosEmpresa;
import ioasys.teste.android.empresas.classes.Enterprise;
import ioasys.teste.android.empresas.classes.Investor;

public class EmpresaDetalhesFragment extends Fragment {

    public static final String EMPRESA = "empresa";

    String id_empresa;

    Context context;

    ImageView imagemEmpresa;
    TextView descricaoEmpresa;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            id_empresa = bundle.getString(EMPRESA, null);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_empresa_detalhes, container, false);

        context = getContext();

        descricaoEmpresa = (TextView) layout.findViewById(R.id.texto_descricao);
        imagemEmpresa = (ImageView) layout.findViewById(R.id.imagem_empresa);

        DetalhesEmpresabyID detalhesEmpresabyID = new DetalhesEmpresabyID();
        detalhesEmpresabyID.execute(id_empresa);

        return layout;
    }

    public static EmpresaDetalhesFragment newInstance(String id) {
        Bundle bundle = new Bundle();
        bundle.putString(EMPRESA, id);

        EmpresaDetalhesFragment empresaDetalhesFragment = new EmpresaDetalhesFragment();
        empresaDetalhesFragment.setArguments(bundle);
        return empresaDetalhesFragment;
    }

//==================================== Inner Classes ===============================================

    class DetalhesEmpresabyID extends AsyncTask<String, Void, DadosEmpresa> {

        String token;
        String client;
        String uid;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            SharedPreferences sharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);
            token = sharedPreferences.getString(Investor.TOKEN, null);
            client = sharedPreferences.getString(Investor.CLIENT, null);
            uid = sharedPreferences.getString(Investor.UID, null);
        }

        @Override
        protected DadosEmpresa doInBackground(String... params) {

            DadosEmpresa dadosEmpresa = null;
            if (params != null && !params[0].isEmpty()) {

                String id = params[0];

                dadosEmpresa = Conexao.buscarDetalhesEmpresaById(token, client, uid, id);
            }

            return dadosEmpresa;
        }

        @Override
        protected void onPostExecute(DadosEmpresa dadosEmpresa) {
            super.onPostExecute(dadosEmpresa);

            if (dadosEmpresa != null) {
                Enterprise enterprise = dadosEmpresa.getEnterprise();

                if (enterprise != null) {

                    EmpresaDetalhesActivity.changeTitleActionBar(getActivity(), enterprise.getEnterpriseName());

                    String descricao = "" + enterprise.getDescription() +
                            "\n\n\nLocalização: " + enterprise.getCity() + ", " + enterprise.getCountry() +
                            "\n\nEmail: " + enterprise.getEmailEnterprise() +
                            "\nTelefone: " + enterprise.getPhone() +
                            "\n\nFacebook: " + enterprise.getFacebook() +
                            "\nLinkedin: " + enterprise.getLinkedin() + "\n";

                    descricaoEmpresa.setText(descricao);

                    if (enterprise.getPhoto() != null) {
                        Glide.with(context)
                                .load(enterprise.getPhoto())
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .override(318, 155)
                                .placeholder(R.drawable.img_e_1)
                                .error(R.drawable.logo_home)
                                .into(imagemEmpresa);
                    }
                }

            }
        }
    }

}
