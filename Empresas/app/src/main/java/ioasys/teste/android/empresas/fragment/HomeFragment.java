package ioasys.teste.android.empresas.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ioasys.teste.android.empresas.MainActivity;
import ioasys.teste.android.empresas.R;

public class HomeFragment extends Fragment {

    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_home, container, false);

        MainActivity.showActionBar(getActivity());

        context = getActivity();

        //TextView t = (TextView) layout.findViewById(R.id.home_text);
//        SharedPreferences sharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);
//        String s = "Token = " + sharedPreferences.getString(Investor.TOKEN, null) +
//                    "\nClient = " + sharedPreferences.getString(Investor.CLIENT, null) +
//                    "\nUID = " + sharedPreferences.getString(Investor.UID, null);
//        t.setText(s);

        return layout;
    }

}
