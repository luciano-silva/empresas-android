package ioasys.teste.android.empresas.classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DadosEmpresa {

    @SerializedName("enterprise")
    @Expose
    private Enterprise enterprise;
    @SerializedName("success")
    @Expose
    private boolean success;

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}