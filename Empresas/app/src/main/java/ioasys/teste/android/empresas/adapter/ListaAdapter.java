package ioasys.teste.android.empresas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import ioasys.teste.android.empresas.R;
import ioasys.teste.android.empresas.classes.EnterpriseLista;

public class ListaAdapter extends BaseAdapter {

    Context context;
    List<EnterpriseLista> lista;

    public ListaAdapter(Context context, List<EnterpriseLista> lista) {
        this.context = context;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_lista, parent, false);

            viewHolder.imagemEmpresa = (ImageView) convertView.findViewById(R.id.empresa_imagem);
            viewHolder.textNomeEmpresa = (TextView) convertView.findViewById(R.id.nome_empresa);
            viewHolder.textTipoEmpresa = (TextView) convertView.findViewById(R.id.tipo_empresa);
            viewHolder.textPaisEmpresa = (TextView) convertView.findViewById(R.id.pais_empresa);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        EnterpriseLista enterprise = lista.get(position);

        if (enterprise.getPhoto() != null) {
            Glide.with(context)
                    .load(enterprise.getPhoto())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(105, 80)
                    .placeholder(R.drawable.img_e_1_lista)
                    .error(R.drawable.logo_home)
                    .into(viewHolder.imagemEmpresa);
        }

        viewHolder.textNomeEmpresa.setText(enterprise.getEnterpriseName());
        viewHolder.textTipoEmpresa.setText(enterprise.getEnterpriseType().getEnterpriseTypeName());
        viewHolder.textPaisEmpresa.setText(enterprise.getCountry());

        return convertView;
    }

    static class ViewHolder {
        ImageView imagemEmpresa;
        TextView textNomeEmpresa;
        TextView textTipoEmpresa;
        TextView textPaisEmpresa;
    }
}
