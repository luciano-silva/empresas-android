package ioasys.teste.android.empresas.classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Investor implements Serializable {

    @SerializedName("investor")
    @Expose
    private Usuario usuario;
    @SerializedName("enterprise")
    @Expose
    private Object enterprise;
    @SerializedName("success")
    @Expose
    private boolean success;

    public static final String TOKEN = "access-token";
    public static final String CLIENT = "client";
    public static final String UID = "uid";

    public static final String CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_TYPE_JSON = "application/json";

    private String header_access_token;
    private String header_client;
    private String header_uid;

    public String getHeader_access_token() {
        return header_access_token;
    }

    public void setHeader_access_token(String header_access_token) {
        this.header_access_token = header_access_token;
    }

    public String getHeader_client() {
        return header_client;
    }

    public void setHeader_client(String header_client) {
        this.header_client = header_client;
    }

    public String getHeader_uid() {
        return header_uid;
    }

    public void setHeader_uid(String header_uid) {
        this.header_uid = header_uid;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Object getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Object enterprise) {
        this.enterprise = enterprise;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}

