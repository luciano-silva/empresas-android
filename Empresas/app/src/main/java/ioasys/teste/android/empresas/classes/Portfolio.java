package ioasys.teste.android.empresas.classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Portfolio {

    @SerializedName("enterprises_number")
    @Expose
    private int enterprisesNumber;
    @SerializedName("enterprises")
    @Expose
    private List<Object> enterprises = null;

    public int getEnterprisesNumber() {
        return enterprisesNumber;
    }

    public void setEnterprisesNumber(int enterprisesNumber) {
        this.enterprisesNumber = enterprisesNumber;
    }

    public List<Object> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Object> enterprises) {
        this.enterprises = enterprises;
    }
}
