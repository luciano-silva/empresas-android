package ioasys.teste.android.empresas.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import ioasys.teste.android.empresas.MainActivity;
import ioasys.teste.android.empresas.R;
import ioasys.teste.android.empresas.classes.Conexao;
import ioasys.teste.android.empresas.classes.Investor;

public class LoginFragment extends Fragment {

    EditText etEmail;
    EditText etSenha;
    Button login;
    ProgressBar progressBar;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_login, container, false);

        context = getContext();
        MainActivity.hideActionBar(getActivity());

        etEmail = (EditText) layout.findViewById(R.id.et_email_login);
        etSenha = (EditText) layout.findViewById(R.id.et_senha_login);
        login = (Button) layout.findViewById(R.id.btn_entrar_login);
        progressBar = (ProgressBar) layout.findViewById(R.id.progressBar_login);


        etEmail.setText("testeapple@ioasys.com.br");
        etSenha.setText("12341234");

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etEmail.setError(null);
                etSenha.setError(null);
                String email = etEmail.getText().toString();
                String senha = etSenha.getText().toString();

                if (!email.isEmpty() && !senha.isEmpty()) {
                    LoginUser loginUser = new LoginUser();
                    loginUser.execute(email, senha);
                } else {
                    if (email.isEmpty()) {
                        etEmail.setError("Você deve informar um email!");
                    } else {
                        etSenha.setError("Você deve informar uma senha!");
                    }
                }

            }
        });

        return layout;
    }

//==================================== Inner Classes ===============================================

    class LoginUser extends AsyncTask<String, Void, Investor> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            login.setVisibility(View.GONE);
        }

        @Override
        protected Investor doInBackground(String... params) {
            return Conexao.fazerLogin(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(Investor investor) {
            super.onPostExecute(investor);
            progressBar.setVisibility(View.GONE);
            login.setVisibility(View.VISIBLE);

            if (investor != null) {

                SharedPreferences sharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.putString(Investor.TOKEN, investor.getHeader_access_token());
                editor.putString(Investor.CLIENT, investor.getHeader_client());
                editor.putString(Investor.UID, investor.getHeader_uid());

                editor.apply();
                editor.commit();

                Activity activity = getActivity();
                if (activity instanceof LoginOnClick) {
                    ((LoginOnClick) activity).login();
                }

            } else {
                etEmail.setError("Login Incorreto!");
            }
        }
    }

//========================================= Interfaces =============================================

    public interface LoginOnClick {
        void login();
    }

}
