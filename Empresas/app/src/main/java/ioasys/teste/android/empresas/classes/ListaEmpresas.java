package ioasys.teste.android.empresas.classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListaEmpresas {

    @SerializedName("enterprises")
    @Expose
    private List<EnterpriseLista> enterprises = null;

    public List<EnterpriseLista> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<EnterpriseLista> enterprises) {
        this.enterprises = enterprises;
    }

}
