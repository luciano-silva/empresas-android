package ioasys.teste.android.empresas.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import ioasys.teste.android.empresas.MainActivity;
import ioasys.teste.android.empresas.R;
import ioasys.teste.android.empresas.adapter.ListaAdapter;
import ioasys.teste.android.empresas.classes.Conexao;
import ioasys.teste.android.empresas.classes.EnterpriseLista;
import ioasys.teste.android.empresas.classes.Investor;
import ioasys.teste.android.empresas.classes.ListaEmpresas;

public class BuscaResultadoFragment extends Fragment {

    ListView listaResultados;
    ProgressBar progressBarBuscar;

    private static final String EMPRESA_BUSCAR = "query_buscar";
    private String buscarEmpresa;
    Context context;

    public static BuscaResultadoFragment newInstance(String queryBuscar) {
        Bundle bundle = new Bundle();
        bundle.putString(EMPRESA_BUSCAR, queryBuscar);

        BuscaResultadoFragment buscaResultadoFragment = new BuscaResultadoFragment();
        buscaResultadoFragment.setArguments(bundle);
        return buscaResultadoFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            buscarEmpresa = bundle.getString(EMPRESA_BUSCAR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        MainActivity.showActionBar(getActivity());

        View layout = inflater.inflate(R.layout.fragment_busca_resultado, container, false);

        context = getContext();

        listaResultados = (ListView) layout.findViewById(R.id.lista_resultados_buscar);
        progressBarBuscar = (ProgressBar) layout.findViewById(R.id.progressBar_buscar);

        if (!buscarEmpresa.isEmpty()) {
            BuscarEmpresasbyNome buscarEmpresasbyNome = new BuscarEmpresasbyNome();
            buscarEmpresasbyNome.execute(buscarEmpresa);
        } else {
            listaResultados.setVisibility(View.GONE);
            progressBarBuscar.setVisibility(View.GONE);
        }

        return layout;
    }

//==================================== Inner Classes ===============================================

    class BuscarEmpresasbyNome extends AsyncTask<String, Void, ListaEmpresas> {

        String token;
        String client;
        String uid;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBarBuscar.setVisibility(View.VISIBLE);
            listaResultados.setVisibility(View.GONE);

            SharedPreferences sharedPreferences = context.getSharedPreferences("login", Context.MODE_PRIVATE);
            token = sharedPreferences.getString(Investor.TOKEN, null);
            client = sharedPreferences.getString(Investor.CLIENT, null);
            uid = sharedPreferences.getString(Investor.UID, null);
        }

        @Override
        protected ListaEmpresas doInBackground(String... params) {

            ListaEmpresas listaEmpresas = null;
            if (params != null && !params[0].isEmpty()) {
                String buscar = params[0];

                listaEmpresas = Conexao.buscarEmpresasByNome(token, client, uid, buscar);

//                String s = "Token = " + token +
//                        "\nClient = " + client +
//                        "\nUID = " + uid + "\n Query=" + buscar;

            } else {

            }

            return listaEmpresas;
        }

        @Override
        protected void onPostExecute(ListaEmpresas empresas) {
            super.onPostExecute(empresas);
            progressBarBuscar.setVisibility(View.GONE);
            listaResultados.setVisibility(View.VISIBLE);

            if (empresas != null && empresas.getEnterprises() != null) {
                ListaAdapter listaAdapter = new ListaAdapter(context, empresas.getEnterprises());
                listaResultados.setAdapter(listaAdapter);
                listaResultados.setOnItemClickListener(onClickEmpresaDetalhesLista());
            }
        }
    }

    private AdapterView.OnItemClickListener onClickEmpresaDetalhesLista() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EnterpriseLista empresa = (EnterpriseLista) parent.getItemAtPosition(position);
                String id_empresa = String.valueOf(empresa.getId());

                Activity activity = getActivity();
                if (activity instanceof BuscarOnClick) {
                    ((BuscarOnClick) activity).itemEscolhidoClique(id_empresa);
                }
            }
        };
    }

//=================================== Interfaces ===================================================
    public interface BuscarOnClick {
        void itemEscolhidoClique(String id);
    }

}
