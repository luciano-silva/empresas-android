package ioasys.teste.android.empresas.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import ioasys.teste.android.empresas.R;
import ioasys.teste.android.empresas.fragment.EmpresaDetalhesFragment;

public class EmpresaDetalhesActivity extends AppCompatActivity {

    FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa_detalhes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String id = getIntent().getStringExtra(EmpresaDetalhesFragment.EMPRESA);

        mFragmentManager = getSupportFragmentManager();

        EmpresaDetalhesFragment empresaDetalhesFragment = EmpresaDetalhesFragment.newInstance(id);

        mFragmentManager.beginTransaction().replace(R.id.frame_fragment, empresaDetalhesFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void changeTitleActionBar(Activity activity, String title) {
        if (activity instanceof EmpresaDetalhesActivity) {
            ActionBar actionBar = ((EmpresaDetalhesActivity) activity).getSupportActionBar();

            if (actionBar != null) {
                actionBar.setTitle(title);
            }
        }
    }

}
